package de.thmshmm.spring.example.one;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * Created by Thomas Hamm on 16.03.19.
 */
@Slf4j
public class MyProducer {

    @Autowired
    private KafkaTemplate<String, String> template;

    private String topic;

    public MyProducer(String topic) {
        this.topic = topic;
    }

    public void send(String message) {
        template.send(topic, message);
        log.info("Sending message: {}", message);
    }
}
