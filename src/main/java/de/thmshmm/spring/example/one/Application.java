package de.thmshmm.spring.example.one;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Thomas Hamm on 16.03.19.
 */
@SpringBootApplication
@Slf4j
public class Application implements CommandLineRunner {

    @Autowired
    private MyProducer producer;

    private Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String input;
        while (true) {
            System.out.print("Enter message ('q' to exit): ");
            input = scanner.nextLine();
            if (input.equals("q")) System.exit(0);
            else producer.send(input);
            Thread.sleep(1000);
        }
    }
}