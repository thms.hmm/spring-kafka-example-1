package de.thmshmm.spring.example.one;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Thomas Hamm on 14.03.19.
 */
@Slf4j
public class MyConsumer {

    private CountDownLatch latch;

    public MyConsumer(int count) {
        this.latch = new CountDownLatch(count);
    }

    @KafkaListener(topics = {"${app.topic}"})
    public void listen(ConsumerRecord<String, String> record) throws Exception {
        log.info("Received message: {}", record.value());
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }
}
