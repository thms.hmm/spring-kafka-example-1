package de.thmshmm.spring.example.one;

import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationConfig.class)
public class ApplicationTests {

    @ClassRule
    public static EmbeddedKafkaRule embeddedKafka = new EmbeddedKafkaRule(1, false, 3, "topic1");

    @Autowired
    MyProducer myProducer;

    @Autowired
    MyConsumer myConsumer;

    @Test
    public void integrationTest() throws InterruptedException {
        myProducer.send("Message1");
        myProducer.send("Message2");
        myConsumer.getLatch().await(10000, TimeUnit.MILLISECONDS);
        Assert.assertTrue(myConsumer.getLatch().getCount() == 0);
    }
}
